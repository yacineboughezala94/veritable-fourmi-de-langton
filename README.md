Voici le jeu de la Fourmi de Langton, la fourmi est posé initialement sur une des cases. Pour que la fourmi connaisse sa position suivante, elle va regarder la couleur de la case sur laquelle elle se trouve.
Si la case est noire, la fourmi tourne de 90 degrés vers la gauche et change la couleur de la case en blanc
Si la case est blanche, la fourmi tourne de 90 degrés vers la droite et change la couleur de la case en noire
